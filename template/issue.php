<?php
include_once("vars.php");
include_once("header.php");
$content = file_get_contents("issues.json");
$issues = json_decode($content, true);
$issue = $issues[$_GET["id"]];

foreach($issue["content"] as $reply) {
	$content = $reply["content"];
	$author = $reply["author"];
	$date = $reply["date"];
	$id = $reply["id"];
	echo "<p>$content</p>";
	echo "<p><i>Message posted the $date by $author (#$id)</i></p>";
}
?>

<form action="issue_reply.php" method="post">
	<p><input type="hidden" name="id" id="id" value="<?=$_GET['id']?>"></p>
	<p><input type="text" name="author" placeholder="Your name here" /></p>
	<p><textarea name="content" placeholder="Type your reply here"></textarea></p>
	<p><input type="submit" value="Reply" /></p>
</form>
