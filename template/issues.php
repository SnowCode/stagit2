<?php
include_once("vars.php");
$json = file_get_contents("issues.json");
$issues = json_decode($json, true);
?>

<?php include("header.php"); ?>
<table id="issues"><thead>
<a href="issue_form.php">Create a new issue</a>
<tr><td><b>ID</b></td><td><b>Commit message</b></td><td><b>Author</b></td><td><b>Latest response</b></td><td><b>Creation date</b></td></tr>
</thead><tbody>
<?php
foreach($issues as $issue) {
	$id = $issue["id"];
	$topic = $issue["topic"];
	$author = $issue["author"];
	$latest = $issue["latest"];
	$date = $issue["date"];
	echo "<tr><td>$id</td><td><a href='issue.php?id=$id'>$topic</a></td><td>$author</td><td>$latest</td><td>$date</td></tr>";
}
?>
</tbody></table></div>
</body>
</html>

