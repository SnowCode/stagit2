<?php
include_once("vars.php");
?>
<!DOCTYPE html>
<html><head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>README - <?=$name?> - <?=$description?>
</title>
<link rel="icon" type="image/png" href="../logo.png">
<link rel='alternate' type='application/atom+xml' title='<?=$name?> Atom Feed' href='https://<?=$host?>/<?=$name?>/atom.xml'>
<link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>

<table><tbody><tr><td><a href='../index.php'><img src='../logo.png' alt='' width='32' height='32'></a></td><td><h1><?=$name?></h1><span class=<?=$description?>
</span></td></tr><tr class='url'><td></td><td>git clone <a href='git://<?=$host?>/<?=$name?>'>git://<?=$host?>/<?=$name?></a></td></tr><tr><td></td><td>
<a href='index.php'>README</a> | <a href='../files/<?=$name?>'>Files</a> | <a href='issues.php'>Issues</a></td></tr></tbody></table>
<hr><div id='content'>
