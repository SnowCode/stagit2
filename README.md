# Stagit 2.0
This is my version of stagit written in PHP and supporting issues and patches. It doesn't require any database, is very simple to install and
very lightweight.

I think it's better to have a lightweight dynamic website than a static website and a bloated email server.

## Roadmap
- [x] Create the templates
- [ ] Copy the templates to the right locations according to the repos in "raw"
- [ ] Get the README
- [ ] Link to the files
- [ ] Create the issues
- [ ] Create the patches




## Installation
Soon...

## License
Soon...
